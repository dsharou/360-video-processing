__author__ = 'daniil'

from multiprocessing import Queue, Process, cpu_count
from poi_utils import *
import json

image_path = "/Users/daniil/Documents/hoankiem"
output_path = "/Users/daniil/Documents/hoan_kiem_TL.json"
image_dimensions = {"width": 1900, "height": 422}

# get number of processors and create buckets with images having a directory
def get_timeline_from_files_list(queue, finder, files):
    queue.put(finder.get_time_line(files))


def get_finder():
    """
    return new ObjectFinder for each process so that they would not interfere with each other
    """
    finder = ObjectFinder(image_path, output_path, image_dimensions)
    finder.debug = True
    return finder


def main():
    buckets = [[] for i in range(0, cpu_count())]
    for root, dirs, files in os.walk(image_path):
        files.sort()
        bucket_counter = 0
        for name in files:
            buckets[bucket_counter].append(name)
            if bucket_counter == cpu_count() - 1:
                bucket_counter = 0
                continue
            else:
                bucket_counter += 1

    result = {}
    q = Queue()
    processes = [Process(target=get_timeline_from_files_list, args=(q, get_finder(), buckets[i])) for i in
                 range(0, cpu_count())]
    for p in processes:
        p.start()

    for i in range(0, cpu_count()):
        result.update(q.get()['frames'])

    final_result = {'frames': [result[k] for k in sorted(result.keys())]}
    fp = open(output_path, "w")
    json.dump(final_result, fp)
    fp.close()
    print output_path, "saved!"

    smooth_operator = SmoothOperator(output_path, output_path.replace(".json", "_smooth.json"))
    smooth_operator.smooth_timeline()


if __name__ == '__main__':
    main()