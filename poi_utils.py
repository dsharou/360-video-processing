__author__ = 'danila'

import os
from os.path import basename, splitext
import json
import cv
from PIL import Image
from color_util.roygbiv import *
import numpy

class ObjectFinder(object):
    debug = False
    log = False
    image_log_folder = "/tmp/"

    def __init__(self, path_to_images, json_output_path, dimensions=None):
        self.path_to_images = path_to_images
        self.json_output_path = json_output_path
        self.dimensions = dimensions

    def get_time_line(self, files = None):
        time_line = {"frames": {}}
        if not files:
            for root, dirs, files in os.walk(self.path_to_images):
                files.sort()
        for name in files:
            try:
                frame = self.get_objects_from_image(self.path_to_images + "/" + name, self.dimensions)
                time_line['frames'][name] = frame
            except IOError as e:
                print "incorrect file type", name
        return time_line

    def save_time_line(self):
        fp = open(self.json_output_path, "w")
        json.dump(self.get_time_line(), fp)
        fp.close()

        smooth_operator = SmoothOperator(self.json_output_path, self.json_output_path.replace(".json", "_smooth.json"))
        smooth_operator.smooth_timeline()

    def rgb_to_hex(self, rgb):
        return '#%02x%02x%02x' % rgb

    def calc_centroid(self, points):
        x, y = zip(*points)
        avg_x, avg_y = round(numpy.mean(x), 6), round(numpy.mean(y) / 2, 6)

        return avg_x, avg_y

    def calc_degree(self, coordinate, dimension, rate=180):
        center = dimension / 2
        result = 0
        if coordinate > center:
            result = ( (coordinate % center) / center ) * rate
        elif coordinate < center:
            result = ( (center - coordinate) / center ) * -rate
        return round(result, 2)

    def get_objects_from_image(self, image_path, image_dimensions=None):
        frame = {'o': {}}
        if self.debug:
            frame['file'] = image_path
            print image_path
        if self.log:
            imcolor = cv.LoadImage(image_path)

        imagePIL = Image.open(image_path)
        pix = imagePIL.load()
        colors = Roygbiv(image_path).get_palette_hex()
        for c in colors:
            if c != '#ffffff':
                frame['o'][c] = []

        img = cv.LoadImageM(image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)
        eig_image = cv.CreateMat(img.rows, img.cols, cv.CV_32FC1)
        temp_image = cv.CreateMat(img.rows, img.cols, cv.CV_32FC1)
        for (x, y) in cv.GoodFeaturesToTrack(img, eig_image, temp_image, 6 * len(colors), 0.04, 3.0, useHarris=True):
            try:
                current_color = self.rgb_to_hex(pix[x, y])
            except Exception:
                current_color = '#ffffff'
            if current_color in frame['o']:
                if image_dimensions:
                    x_degree = self.calc_degree(x, image_dimensions["width"])
                    y_degree = self.calc_degree(y, image_dimensions["height"], 90)
                    frame['o'][current_color].append((x_degree, y_degree))
                else:
                    frame['o'][current_color].append((x, y))

        corrupted_frames = []
        for c in frame['o']:
            obj = frame['o'][c]
            if len(obj) > 0:
                top_most = sorted(obj, key=itemgetter(1))[:2]
                top_most = sorted(top_most, key=itemgetter(0), reverse=True)
                bottom_most = sorted(obj, key=itemgetter(1))[2:]
                bottom_most = sorted(bottom_most, key=itemgetter(0))
                frame['o'][c] = top_most + bottom_most
                if len(frame['o'][c]) < 4:
                    if len(frame['o'][c]) == 3:
                        print image_path, " COVERABLE error!"
                    elif len(frame['o'][c]) < 3:
                        print image_path, " UNCOVERABLE error!"
                        corrupted_frames.append(c)
                        continue
                if self.log:
                    center_x, center_y = self.calc_centroid(frame['o'][c])
                    cv.Circle(imcolor, (int(center_x), int(center_y * 2)), 2, cv.RGB(155, 0, 25), 2)
                frame['o'][c] = self.calc_centroid(frame['o'][c])
            else:
                corrupted_frames.append(c)

        for cf in corrupted_frames:
            del(frame['o'][cf])

        if self.log:
            cv.SaveImage(self.image_log_folder + basename(image_path), imcolor)

        return frame

class SmoothOperator(object):
    def __init__(self, path_from, path_to):
        self.path_from = path_from
        self.path_to = path_to

    def get_object_frames(self, time_line):
        frames = time_line['frames']
        object_frames = {}
        frame_counter = 0
        for f in frames:
            objects = f['o']
            for color in objects:
                if color not in object_frames:
                    object_frames[color] = {}

                object_frames[color][frame_counter] = objects[color]
            frame_counter += 1

        return object_frames

    def smooth_operator(self, sequence, frames):
        first_frame = frames[sequence[0]]
        last_frame = frames[sequence[len(sequence) - 1]]

        start_x, start_y = first_frame
        end_x, end_y = last_frame
        step_x = (end_x - start_x) / (len(sequence) - 1)
        step_y = (end_y - start_y) / (len(sequence) - 1)

        processed_frames = {}
        x_counter = start_x
        y_counter = start_y
        for s in sequence[0:len(sequence)]:
            processed_frames[s] = [round(x_counter, 6), round(y_counter, 6)]
            y_counter += step_y
            x_counter += step_x

        return processed_frames

    def get_smooth_object(self, object_frames):
        keys = sorted(object_frames.keys())
        keys_length = len(keys)

        sequence = list()
        sequence.append(keys[0:int(keys_length / 3)])
        sequence.append(keys[int(keys_length / 3):int(keys_length / 3) * 2])
        sequence.append(keys[int(keys_length / 3) * 2: keys_length])

        object_frames_processed = {}
        for i in sequence:
            object_frames_processed = dict(object_frames_processed.items() + self.smooth_operator(i, object_frames).items())

        return object_frames_processed

    def smooth_timeline(self):
        try:
            fp = open(self.path_from, "r")
        except IOError as e:
            print e.message
            exit()

        base_json = json.load(fp)
        object_frames = self.get_object_frames(base_json)

        objects_processed = {}
        for color in object_frames:
            objects_processed[color] = self.get_smooth_object(object_frames[color])

        for color in objects_processed:
            for frame in objects_processed[color]:
                base_json['frames'][frame]['o'][color] = objects_processed[color][frame]

        save_fp = open(self.path_to, "w")
        json.dump(base_json, save_fp)
        save_fp.close()

        print self.path_to, "saved!"